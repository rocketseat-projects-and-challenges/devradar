# DevRadar

Código Fonte desenvolvido durante a semana Omnistack 10.0

![alt text](https://gitlab.com/rocketseat-projects-and-challenges/devradar/raw/a0e66264d0541d9fe8b2e42647985a350a06887a/images/web.png)
![alt text](https://gitlab.com/rocketseat-projects-and-challenges/devradar/raw/a0e66264d0541d9fe8b2e42647985a350a06887a/images/mobMain.png)
![alt text](https://gitlab.com/rocketseat-projects-and-challenges/devradar/raw/a0e66264d0541d9fe8b2e42647985a350a06887a/images/mobUser.png)

## Arquitetura da Aplicação

### Back-End

- Regras de Negócio;
- Conexão com Banco de Dados;
- Comunicação com Webservices;

### Front-End

- Front-End WEB ( React )
- Front-End Mobile ( React Native )

### Serviços

- Serviços Externos

### Formato de Comunicação

- JSON

### Banco de Dados

- MongoDB ( Banco Não Relacional ) - https://www.mongodb.com/cloud/atlas

## BackEnd

- Iniciar o Projeto de Backend

```
$ yarn init -y
```

- Instalar Express

```
$ yarn add express
```

- Instalar o Nodemon

```
$ yarn add nodemon -D
```

> Instala a dependência do nodemon em modo de desenvolvimento

- Instalar Dependencia do Mongoose

```
$ yarn add mongoose
```

- Instalar Dependência do Axios

```
$ yarn add axios
```

> Axios é uma biblioteca que faz chamadas para outras apis disponíveis

- Instala a dependencia Cors

```
  $yarn add cors
```

## Front-End

- Abordagem SPA ( Single-page applications ) : Na abordagem de SPA, as requisições trazem apenas dados como respostas e com esses dados o fron-end pode preencher as informações em tela. A página nunca recarrega, otimizando a performance e dando vida ao conceito de SPA. Retornando apenas JSON podemos ter quantos front-ends quisermos.

- Iniciando o Projeto :

```
$ yarn create react-app web
```

- Iniciando a Aplicação :

```
$ yarn start
```

- Instalar Dependência do Axios

```
$ yarn add axios
```

## Mobile

- Todo código feito é em JavaScript, esse código não é convertido em código nativo, melhor do que isso, o dispositivo passa a entender o código JavaScript e a interface gerada é totalmente nativa.

- Expo : é um framework para React Native ( conjunto de ferramentas e bibliotecas )

- Instalação do Expo

```
$ yarn global add expo-cli
```

- Subir o projeto

```
$ yarn start
```

- Instalação do React Navigation

```
$ yarn add react-navigation
$ expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
$ yarn add react-navigation-stack
$ yarn add @react-native-community/masked-view

```

- Instalação do React Native Maps

```
$ expo install react-native-maps
```

- Instalação do EXPO Location

```
$ expo install expo-location
```
